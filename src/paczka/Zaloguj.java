package paczka;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Zaloguj extends JFrame {
	public static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";
	private static Connection connection = null;

	private JTextField pole_nick;
	private JTextField pole_haslo;
	private JLabel nick = new JLabel("Podaj pseudonim");
	private JLabel haslo = new JLabel("Podaj haslo");
	private JPanel panel = new JPanel();
	private JButton zaloguj = new JButton("Zaloguj");

	private String nick_s;
	private String haslo_s;
	private boolean wynik = false;
	public static int id_g = -1;

	public static int getId_g() {
		return id_g;
	}

	private void loguj() {
		pole_nick = new JTextField();
		pole_nick.setPreferredSize(new Dimension(100, 20));
		pole_haslo = new JTextField();
		pole_haslo.setPreferredSize(new Dimension(100, 20));

	}

	private void spr() {
		nick_s = pole_nick.getText();
		haslo_s = pole_haslo.getText();

	}

	private boolean sprawdzanie() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT id_konta FROM gracz WHERE nick_gracza='"
							+ nick_s + "' and haslo='" + haslo_s + "'");
			while (resultSet.next()) {
				id_g = resultSet.getInt(1);
			}
			if (id_g != -1) {
				wynik = true;
				JOptionPane.showMessageDialog(null,
						"Udalo ci si� zalogowa� na konto " + nick_s);
			} else {
				JOptionPane.showMessageDialog(null, "Z�y login lub has�o");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return wynik;
	}

	void panel() {
		panel.setPreferredSize(new Dimension(200, 200));
		panel.setVisible(true);
		this.loguj();
		panel.add(nick);
		panel.add(pole_nick);
		panel.add(haslo);
		panel.add(pole_haslo);
		panel.add(zaloguj);
		zaloguj.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				spr();
				if (sprawdzanie() == true) {
					Garaz nowy = new Garaz(connection);
					pole_nick.setText("");
					pole_haslo.setText("");
					JFrame topFrame = (JFrame) SwingUtilities
							.getWindowAncestor(zaloguj);
							topFrame.dispatchEvent(new WindowEvent(topFrame,
							WindowEvent.WINDOW_CLOSING));
					panel.repaint();
					panel.revalidate();
				}
			}
		});	
	}

	public Zaloguj(Connection pol) {
		this.connection = pol;
		this.setResizable(false);
		setSize(400, 120);
		this.setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.panel();
		this.spr();
		this.getContentPane().add(panel);
		
	}
}
