package paczka;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import quick.dbtable.DBTable;

public class Zakup_pojazdu extends JFrame {
	private DBTable grid;
	public static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";
	private static Connection connection;
	private JComboBox<Czolg> nacja;
	private JComboBox<Czolg> typ;
	private JComboBox<Czolg> kupno = new JComboBox<Czolg>();

	private JPanel panelKupna;
	private JPanel panelPrzyciskow;

	private JButton kup = new JButton("Kup");
	private JButton anuluj = new JButton("Anuluj");
	private JButton dalej = new JButton("Dalej");

	private JLabel nacjaL = new JLabel("Wybierz nacje");
	private JLabel typL = new JLabel("Wybierz typ pojazdu");
	private JLabel srebroL = new JLabel("Srebro");
	private JLabel expL = new JLabel("Do�wiadczenie");
	private JLabel kupnoLa = new JLabel("Wybierz pojazd do kupienia");
	private JLabel pomoc = new JLabel("Po wybraniu wci�nij przycisk dalej");
	private JTextField exp;
	private JTextField srebro;

	private int pkt_wytrz;
	private int dzialo;
	private int pancerz;
	private int przeladunek;

	private int ilosc_srebra;
	private int ilosc_exp;
	private int ilosc_s;
	private int ilosc_e;
	private String czolg;

	/*
	 * po��czenie
	 * 
	 * @return true je�li udalo si� po��czy�, false w przeciwnym wypadku
	 */
	private boolean setConnection(String databaseLocation, String user,
			String password) {
		try {
			Class.forName(Zakup_pojazdu.JDBC_DRIVER);
			connection = DriverManager.getConnection(databaseLocation, user,
					password);
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "B�ad po�aczenia z baza", "",
					JOptionPane.OK_OPTION);
			return false;
		}
	}

	/*
	 * pobiera nacje z bazy danych do ComboBox
	 */
	void pobierzNacje() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT distinct(nacja) FROM nacja");
			nacja = new JComboBox<Czolg>();
			nacja.setPreferredSize(new Dimension(100, 15));
			while (resultSet.next()) {
				String nacjaN = resultSet.getString(1);
				nacja.addItem(new Czolg(nacjaN));
			}
			nacja.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					nacja.getSelectedItem();
				}

			});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"B��d po��czenia z baz� danych, tabela nacja");
		}
	}

	/*
	 * pobiera typ pojazdu z bazy danych do ComboBox
	 */
	void pobierzTyp() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT klasa FROM klasa");
			typ = new JComboBox<Czolg>();
			typ.setPreferredSize(new Dimension(100, 15));
			while (resultSet.next()) {
				String typL = resultSet.getString(1);
				typ.addItem(new Czolg(typL));
			}
			typ.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					typ.getSelectedItem();

				}

			});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"B��d po��czenia z typami czo�g�w");
		}
	}

	/*
	 * pobiera ilo�� srebra z konta u�ytkownika
	 */
	void pobierzSrebro() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT srebro FROM zasob WHERE id="
							+ Zaloguj.getId_g());
			srebro = new JTextField();
			srebro.repaint();
			srebro.setPreferredSize(new Dimension(100, 20));
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				srebro.setText(text);
				srebro.setEditable(false);
				ilosc_srebra = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B�ad ze srebrem");
		}
	}

	/*
	 * pobiera zasoby z konta uzytkownika
	 */
	void pobierzZasoby() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT doswiadczenie FROM zasob WHERE id="
							+ Zaloguj.getId_g());
			exp = new JTextField();
			exp.setPreferredSize(new Dimension(100, 20));
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				exp.setText(text);
				exp.setEditable(false);
				ilosc_exp = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"B�ad z zlokalizowaniem posiadanego doswiadczenia", "",
					JOptionPane.OK_OPTION);
		}
	}

	/*
	 * wyswietla w comboBox pojazdy mo�liwe do zakupienia przez uzytkownika
	 */
	void wyswietlPojazdy() {
		try {
			
			Statement statement = connection.createStatement();
			Czolg g = (Czolg) nacja.getSelectedItem();
			Czolg k = (Czolg) typ.getSelectedItem();
			kupno.removeAllItems();
			ResultSet resultSet = statement
					.executeQuery("SELECT distinct m.nazwa FROM model m, zasob z WHERE m.srebro <= z.srebro and m.nacja='"
							+ g.getNacja()
							+ "'and m.klasa='"
							+ k.getNacja()+"' and m.doswiadczenie <= z.doswiadczenie"
							+ " and z.id="+Zaloguj.id_g);

			kupno.setPreferredSize(new Dimension(100, 50));
			while (resultSet.next()) {
				String kupnoL = resultSet.getString(1);
				kupno.addItem(new Czolg(kupnoL));
			}
			kupno.repaint();
			kupno.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					kupno.getSelectedItem();
				}

			});

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B�ad z wyswietleniem", "",
					JOptionPane.OK_OPTION);
		}
	}

	/*
	 * wyswietla tabele z czo�gami jakie moze kupi� u�ytkownik
	 */
	void wyswietlTabele() {
		String zapytanie = "SELECT distinct m.nazwa,m.srebro,m.doswiadczenie,m.klasa,m.nacja "
				+ "FROM model m WHERE  m.srebro is not null ORDER BY m.srebro";
		grid.setSelectSql(zapytanie);

		try {
			grid.refresh();
			grid.setEditable(false);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B�ad po�aczenia z baza", "",
					JOptionPane.OK_OPTION);
		}
	}

	private int losowo() {
		Random r = new Random();
		pkt_wytrz = r.nextInt(1000) + 500;
		return pkt_wytrz;

	}

	private int losowoDzialo() {
		Random r = new Random();
		dzialo = r.nextInt(1000) + 30;
		return dzialo;
	}

	private int losowoPancerz() {
		Random r = new Random();
		pancerz = r.nextInt(200) + 10;
		return pancerz;
	}

	private int losowyPrzeladunek() {
		Random r = new Random();
		przeladunek = r.nextInt(20) + 2;
		return przeladunek;
	}

	/*
	 * panel wyboru
	 */
	void zaznaczanie() {

		panelKupna = new JPanel();
		panelKupna.setPreferredSize(new Dimension(300, 160));
		GridLayout nowy = new GridLayout(6, 2, 10, 10);
		panelKupna.setLayout(nowy);
		this.pobierzNacje();
		panelKupna.add(nacjaL);
		panelKupna.add(nacja);
		this.pobierzTyp();
		panelKupna.add(typL);
		panelKupna.add(typ);
		this.pobierzSrebro();
		panelKupna.add(srebroL);
		panelKupna.add(srebro);
		this.pobierzZasoby();
		panelKupna.add(expL);
		panelKupna.add(exp);

	}

	/*
	 * panel przycisk�w
	 */
	private void przyciski() {
		panelPrzyciskow = new JPanel();
		panelPrzyciskow.setPreferredSize(new Dimension(300, 40));
		GridLayout uklad = new GridLayout(0, 2);
		panelPrzyciskow.setLayout(uklad);
		panelPrzyciskow.add(anuluj);
		anuluj.setAction(new AbstractAction() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame topFrame = (JFrame) SwingUtilities
						.getWindowAncestor(anuluj);
				topFrame.dispatchEvent(new WindowEvent(topFrame,
						WindowEvent.WINDOW_CLOSING));
				Garaz nowy = new Garaz(connection);
			}
		});
		anuluj.setText("Zako�cz");
		panelPrzyciskow.add(kup);
		kup.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent g) {
				try {
					Czolg c = (Czolg) kupno.getSelectedItem();
					czolg = c.getNacja();
					Statement statement2 = connection.createStatement();
					ResultSet zapytanie2 = statement2
							.executeQuery("SELECT m.srebro,m.doswiadczenie FROM zasob z, model m WHERE z.id="
									+ Zaloguj.getId_g()
									+ " AND m.nazwa='"
									+ czolg + "'");
					while (zapytanie2.next()) {
						ilosc_s = zapytanie2.getInt(1);
						ilosc_e = zapytanie2.getInt(2);
					}
					if (ilosc_srebra >= ilosc_s) {

						String zapytanie = "INSERT into CZOLG (punkty_wytrzymalosci,nazwa,dzialo,moc_silnika,pancerz,przeladowanie,id_konta) VALUES(?,?,?,?,?,?,?)";
						PreparedStatement statement = connection
								.prepareStatement(zapytanie);
						statement.setInt(1, losowo());
						statement.setObject(2, c.getNacja());
						statement.setInt(3, losowoDzialo());
						statement.setInt(4, losowoDzialo());
						statement.setInt(5, losowoPancerz());
						statement.setInt(6, losowyPrzeladunek());
						statement.setInt(7, Zaloguj.getId_g());
						statement.executeUpdate();

						ilosc_srebra = ilosc_srebra - ilosc_s;
						ilosc_exp = ilosc_exp - ilosc_e;

						Statement statement3 = connection.createStatement();
						String zapytanie3 = "UPDATE zasob SET srebro="
								+ ilosc_srebra + ",doswiadczenie=" + ilosc_exp
								+ " WHERE id=" + Zaloguj.getId_g() + " ";
						int result = statement3.executeUpdate(zapytanie3);
						JOptionPane.showMessageDialog(kup,
								"Uda�o si� zakupi� pojazd");
					} else {
						JOptionPane.showMessageDialog(null,
								"Nie mo�esz mie� ujemnego salda konta");
					}

				} catch (SQLException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(kup,
							"Nie uda�o si� zakupi� pojazdu", "",
							JOptionPane.OK_OPTION);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(kup, "Niepoprawne dane", "",
							JOptionPane.OK_OPTION);
				}
			}
		});
	}

	private void panelowy() {

		panelKupna.add(pomoc);
		panelKupna.add(dalej);
		dalej.setPreferredSize(new Dimension(10, 10));
		dalej.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				wyswietlPojazdy();
				panelKupna.revalidate();
				panelKupna.add(kupnoLa);
				panelKupna.add(kupno);
			}
		});

	}

	/*
	 * konstruktor
	 */
	public Zakup_pojazdu(Connection pol) {
		this.connection = pol;
		setSize(680, 430);
		this.setResizable(false);
		setVisible(true);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.zaznaczanie();
		this.getContentPane().add(panelKupna, BorderLayout.NORTH);
		this.panelowy();
		grid = new DBTable();
		grid.setPreferredSize(new Dimension(590, 500));
		this.getContentPane().add(grid, BorderLayout.WEST);
		if (this.setConnection(
				"jdbc:firebirdsql://localhost:3050/C:/Users/KTOSIEK/Desktop/projekt_World_of/MILITARIA",
				"sysdba", "masterkey")) {
			grid.setConnection(connection);
			this.wyswietlTabele();
		}
		this.przyciski();
		this.getContentPane().add(panelPrzyciskow, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.pack();
	}
}
