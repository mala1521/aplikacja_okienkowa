package paczka;

public class Klan {
	private String nazwa;
	
	public Klan(String nazwa){
		this.nazwa=nazwa;
	}
	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String toString() {
		return nazwa;
	}
}
