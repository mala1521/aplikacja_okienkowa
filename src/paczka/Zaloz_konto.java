package paczka;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Zaloz_konto extends JFrame {

	public static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";
	private static Connection connection;
	private JComboBox<Serwer> serwer;
	private JPanel panelFormularz;
	private JPanel panelAkcja;

	private JTextField pole_nick = new JTextField();
	private JPasswordField pole_haslo = new JPasswordField();
	private JPasswordField pole_phaslo = new JPasswordField();
	private JTextField pole_e_mail = new JTextField();
	private JTextField pole_kraj = new JTextField();

	private JLabel email = new JLabel("Podaj e-mail");
	private JLabel nick = new JLabel("Podaj pseudonim");
	private JLabel haslo = new JLabel("Podaj haslo");
	private JLabel haslo_p = new JLabel("Powtorz haslo");
	private JLabel kraj = new JLabel("Kraj");
	private JLabel serwerp = new JLabel("Wybierz serwer");

	private JButton dodaj = new JButton("Dodaj");
	private JButton anuluj = new JButton("Anuluj");
	int id;

	/*
	 * @return zwraca aktualn� date
	 */
	public static String now(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}

	void pobierzSerwery() {

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT id,nazwa FROM serwer ");
			serwer = new JComboBox<Serwer>();
			serwer.setPreferredSize(new Dimension(100, 15));
			while (resultSet.next()) {
				String nazwa = resultSet.getString(2);
				int idserwera = resultSet.getInt(1);
				serwer.addItem(new Serwer(nazwa, idserwera));

			}

			serwer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent actionEvent) {
					serwer.getSelectedItem();

				}
			});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"B��d po��czenia z baz� danych!!!!!!!!!", "",
					JOptionPane.OK_OPTION);
		}
	}

	/*
	 * @return haslo podane przez u�ytkownika
	 */
	public String getHaslo() {
		String haslo = "";
		char[] has = pole_haslo.getPassword();
		for (int i = 0; i < has.length; i++) {
			haslo += has[i];
		}
		return haslo;
	}

	/*
	 * @return haslo podane przez u�ytkownika
	 */
	public String getPHaslo() {
		String haslo = "";
		char[] has = pole_phaslo.getPassword();
		for (int i = 0; i < has.length; i++) {
			haslo += has[i];
		}
		return haslo;
	}

	/*
	 * sprawdza czy podane w formularzu has�a s� identyczne
	 * 
	 * @return true je�li has�a s� identyczne, false w przeciwnym wypadku
	 */
	public boolean poprawnosc() {
		boolean pop_hasla = false;

		if (getPHaslo().equals(getHaslo())) {
			pop_hasla = true;
		} else {
			JOptionPane.showMessageDialog(null, "Hasla roznia sie");
		}
		return pop_hasla;
	}

	private void przygotujFormularz() {

		panelFormularz = new JPanel();
		panelFormularz.setPreferredSize(new Dimension(320, 160));
		GridLayout uklad = new GridLayout(6, 2, 10, 10);
		panelFormularz.setLayout(uklad);
		panelFormularz.add(email);
		panelFormularz.add(pole_e_mail);
		panelFormularz.add(nick);
		panelFormularz.add(pole_nick);
		panelFormularz.add(haslo);
		panelFormularz.add(pole_haslo);
		panelFormularz.add(haslo_p);
		panelFormularz.add(pole_phaslo);
		panelFormularz.add(kraj);
		panelFormularz.add(pole_kraj);
		this.pobierzSerwery();
		panelFormularz.add(serwerp);
		panelFormularz.add(serwer);
	}

	private void przycisk() {
		panelAkcja = new JPanel();
		panelAkcja.setPreferredSize(new Dimension(320, 40));
		GridLayout uklad = new GridLayout(0, 2);
		panelAkcja.setLayout(uklad);
		panelAkcja.add(anuluj);
		anuluj.setAction(new AbstractAction() {

			public void actionPerformed(ActionEvent arg0) {

				JFrame topFrame = (JFrame) SwingUtilities
						.getWindowAncestor(anuluj);
				topFrame.dispatchEvent(new WindowEvent(topFrame,
						WindowEvent.WINDOW_CLOSING));
			}
		});
		anuluj.setText("Anuluj");
		panelAkcja.add(dodaj);
		dodaj.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Connection connection = null;
				try {
					if (poprawnosc()) {

						Class.forName(Zaloz_konto.JDBC_DRIVER);
						connection = DriverManager
								.getConnection(
										"jdbc:firebirdsql://localhost:3050/C:/Users/KTOSIEK/Desktop/projekt_World_of/MILITARIA",
										"sysdba", "masterkey");
						Statement statement = null;

						connection.setAutoCommit(true);
						statement = connection.createStatement();
						Serwer s = (Serwer) serwer.getSelectedItem();
						String zapytanie = "INSERT INTO GRACZ (nick_gracza,e_mail,haslo,data_zalozenia,kraj,id_serwer) VALUES('"
								+ pole_nick.getText()
								+ "', '"
								+ pole_e_mail.getText()
								+ "','"
								+ getHaslo()
								+ "','"
								+ now("yyyy-MM-dd")
								+ "','"
								+ pole_kraj.getText()
								+ "','"
								+ s.getId_serwer() + "')";

						int resultSet = statement.executeUpdate(zapytanie);
						ResultSet resultSet1 = statement
								.executeQuery("SELECT id_konta FROM gracz WHERE nick_gracza= '"
										+ pole_nick.getText() + "' ");
						while (resultSet1.next()) {
							id = resultSet1.getInt(1);
						}
						String zapytanie3 = "INSERT INTO zasob (id,srebro,zloto,doswiadczenie,wolne_doswiadczenie) VALUES ("
								+ id + ",10000000,100,1000000,1000)";
						int resultSet2 = statement.executeUpdate(zapytanie3);
						JOptionPane
								.showMessageDialog(dodaj,
										"Uda�o si� za�o�y� konto oraz otrzyma�e� pakiet startowy");
						pole_haslo.setText("");
						pole_nick.setText("");
						pole_phaslo.setText("");
						pole_e_mail.setText("");
						pole_kraj.setText("");
						JFrame topFrame = (JFrame) SwingUtilities
								.getWindowAncestor(dodaj);
						topFrame.dispatchEvent(new WindowEvent(topFrame,
								WindowEvent.WINDOW_CLOSING));
					}

				} catch (SQLException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(dodaj,
							"Nie uda�o si� zapisa� danych w bazie", "",
							JOptionPane.OK_OPTION);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Zaloz_konto(Connection pol) {
		this.connection = pol;
		setSize(500, 500);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.przygotujFormularz();
		this.getContentPane().add(panelFormularz);
		this.przycisk();
		this.getContentPane().add(BorderLayout.SOUTH, panelAkcja);
		this.pack();
	}
}
