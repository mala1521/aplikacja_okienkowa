package paczka;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Okno extends JFrame {
	public static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";

	private static Connection connection = null;
	quick.dbtable.DBTable dBTable1 = new quick.dbtable.DBTable();

	private JPanel panel;
	private JPanel panel1;
	private JPanel przyciska;
	private JLabel label;
	private JButton wybor1;
	private JButton wybor2;
	public JButton anuluj;

	private boolean setConnection(String databaseLocation, String user,
			String password) {
		try {
			Class.forName(Okno.JDBC_DRIVER);
			connection = DriverManager.getConnection(databaseLocation, user,
					password);
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "B�ad po�aczenia z baza", "",
					JOptionPane.OK_OPTION);
			return false;
		}
	}

	public void menu(String jdbcDriver) {
		wybor1 = new JButton("Za�� konto w grze");
		wybor1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Zaloz_konto nowy = new Zaloz_konto(connection);
				nowy.pobierzSerwery();
				panel.removeAll();
				setVisible(false);
			}
		});
		wybor2 = new JButton("Zaloguj si�");
		wybor2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Zaloguj nowy = new Zaloguj(connection);
				panel.removeAll();
				setVisible(false);
			}
		});
	}

	private void panel() {
		panel = new JPanel();
		panel1 = new JPanel();
		przyciska = new JPanel();
		label = new JLabel(
				"<html><center><b><u><h1>Witamy</h1></u></b></center>\n\n"
						+ "<center>Je�li jeste� nowym u�ytkownikiem kliknij opcje za�� konto</center>"
						+ "\n<center>Je�li odwiedzasz nas ponownie zaloguj si�\n</center>");
		panel.setPreferredSize(new Dimension(350, 200));
		GridLayout nowy1 = new GridLayout(6, 2, 10, 10);
		panel.setLayout(nowy1);
		panel1.add(label);
		panel1.setPreferredSize(new Dimension(350, 100));
		GridLayout nowy = new GridLayout(6, 2, 10, 10);
		panel.setLayout(nowy);
		this.menu(JDBC_DRIVER);
		panel.add(wybor1);
		panel.add(wybor2);
		przyciska.setPreferredSize(new Dimension(350, 50));
		GridLayout nowy2 = new GridLayout(1,3,1,1);
		przyciska.setLayout(nowy2);
		anuluj = new JButton("Zako�cz");
		przyciska.add(anuluj);
		
		anuluj.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				JFrame topFrame = (JFrame) SwingUtilities
						.getWindowAncestor(anuluj);
				topFrame.dispatchEvent(new WindowEvent(topFrame,
						WindowEvent.WINDOW_CLOSING));
			}
		});
		
	}

	public Okno(String databaseLocation, String user, String password) {
		setSize(350, 100);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		setVisible(true);
		this.panel();
		this.setConnection(databaseLocation, user, password);
		this.getContentPane().add(BorderLayout.NORTH, panel1);
		this.getContentPane().add(BorderLayout.CENTER, panel);
		this.getContentPane().add(BorderLayout.SOUTH, przyciska);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.pack();
	}

	public static void main(String[] args) {

		Okno myframe = new Okno(
				"jdbc:firebirdsql://localhost:3050/C:/Users/KTOSIEK/Desktop/projekt_World_of/MILITARIA",
				"sysdba", "masterkey");

		myframe.menu(JDBC_DRIVER);
	}

}
