package paczka;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import quick.dbtable.DBTable;

public class Garaz extends JFrame {
	public static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";
	private static Connection connection = null;

	private JLabel srebro = new JLabel("Ilo�� srebra:");
	private JLabel exp = new JLabel("Ilo�� do�wiadczenia:");
	private JLabel expw = new JLabel("Ilo�� wolnego do�wiadczenia:");
	private JLabel klan = new JLabel("Tw�j klan:");
	private JLabel pomoc = new JLabel("");
	private JLabel klanowy = new JLabel("Do��cz/zmie� klan");
	private JLabel opis = new JLabel("Kliknij przycisk je�li chcesz kupi� czo�g");
	private JButton kup;
	private JButton dolacz = new JButton("Do��cz");
	private JComboBox<Klan> nazwa;
	private JTextField srebroT;
	private JTextField expT;
	private JTextField expwT;
	private JTextField klanT;
	private int ilosc_srebra;
	private int ilosc_exp;
	private int ilosc_expw;
	private int id;
	private DBTable grid;
	private JPanel panel;

	public void wyswietlGaraz() {
		String zapytanie = "SELECT nazwa,doswiadczenie,przeladowanie,pancerz FROM czolg  WHERE id_konta="
				+ Zaloguj.id_g;
		grid.setSelectSql(zapytanie);
		try {
			grid.refresh();
			grid.setEditable(false);

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B��d po��czenia z baz�", "",
					JOptionPane.OK_OPTION);
		}
	}

	void pobierzSrebro() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT srebro FROM zasob WHERE id="
							+ Zaloguj.getId_g());
			srebroT = new JTextField();
			srebroT.repaint();
			srebroT.setPreferredSize(new Dimension(50, 20));
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				srebroT.setText(text);
				srebroT.setEditable(false);
				ilosc_srebra = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B��d z pobraniem srebra");
		}
	}

	void pobierzexp() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT doswiadczenie FROM zasob WHERE id="
							+ Zaloguj.getId_g());
			expT = new JTextField();
			expT.setPreferredSize(new Dimension(100, 20));
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				expT.setText(text);
				expT.setEditable(false);
				ilosc_exp = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B��d, co� posz�o nie tak :/",
					"", JOptionPane.OK_OPTION);
		}
	}

	void pobierzWexp() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT wolne_doswiadczenie FROM zasob WHERE id="
							+ Zaloguj.getId_g());
			expwT = new JTextField();
			expwT.setPreferredSize(new Dimension(100, 20));
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				expwT.setText(text);
				expwT.setEditable(false);
				ilosc_expw = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B��d, co� posz�o nie tak :/",
					"", JOptionPane.OK_OPTION);
		}
	}

	void pobierzKlan() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT k.nazwa FROM klan k join gracz g on k.id_klanu=g.id_klanu WHERE g.id_konta="
							+ Zaloguj.id_g);
			klanT = new JTextField();
			klanT.setPreferredSize(new Dimension(100, 20));
			klanT.setEditable(false);
			while (resultSet.next()) {
				String text = resultSet.getString(1);
				klanT.setText(text);
				
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"B��d z odnalezieniem Twojego klanu");
		}
	}

	void szukajKlanu() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT nazwa FROM klan");

			nazwa = new JComboBox<Klan>();
			nazwa.setPreferredSize(new Dimension(100, 20));
			while (resultSet.next()) {
				String nazwa1 = resultSet.getString(1);
				nazwa.addItem(new Klan(nazwa1));
			}
			nazwa.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					id = nazwa.getSelectedIndex() + 1;
					nazwa.getSelectedItem();
				}

			});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "B��d z odnalezieniem klanu");
		}
	}

	void panelo() {
		panel = new JPanel();
		panel.setSize(500, 500);
		GridLayout nowy = new GridLayout(7, 2, 10, 10);
		panel.setLayout(nowy);
		this.pobierzSrebro();
		panel.add(srebro);
		panel.add(srebroT);
		this.pobierzexp();
		panel.add(exp);
		panel.add(expT);
		this.pobierzWexp();
		panel.add(expw);
		panel.add(expwT);
		this.pobierzKlan();
		panel.add(klan);
		panel.add(klanT);
		panel.add(opis);
		kup = new JButton("Zakup nowy pojazd");
		panel.add(kup);
		this.szukajKlanu();
		panel.add(klanowy);
		panel.add(nazwa);	
		panel.add(pomoc);
		panel.add(dolacz);
		dolacz.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent m) {
				try {
					Statement statement = connection.createStatement();
					String zapytanie = "UPDATE gracz SET id_klanu ="+id+ "WHERE id_konta="+Zaloguj.id_g;
					int result = statement.executeUpdate(zapytanie);
					JOptionPane.showMessageDialog(dolacz, "Do��czy�e� do klanu :)");
					panel.remove(klanowy);
					panel.remove(dolacz);
					panel.remove(nazwa);
					panel.revalidate();
				} catch (SQLException n) {
					n.printStackTrace();
				}
			}

		});

		kup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Zakup_pojazdu nowy = new Zakup_pojazdu(connection);
				JFrame topFrame = (JFrame) SwingUtilities
						.getWindowAncestor(kup);
						topFrame.dispatchEvent(new WindowEvent(topFrame,
						WindowEvent.WINDOW_CLOSING));
			}
		});
	}

	private boolean setConnection(String databaseLocation, String user,
			String password) {
		try {
			Class.forName(Garaz.JDBC_DRIVER);
			connection = DriverManager.getConnection(databaseLocation, user,
					password);
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "B��d po��czenia z baz�", "",
					JOptionPane.OK_OPTION);
			return false;
		}
	}

	Garaz(Connection pol) {
		this.connection = pol;
		this.setSize(500, 450);
		this.setVisible(true);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.panelo();
		this.getContentPane().add(panel, BorderLayout.NORTH);
		grid = new DBTable();

		grid.setPreferredSize(getMaximumSize());
		this.getContentPane().add(grid, BorderLayout.CENTER);
		if (this.setConnection(
				"jdbc:firebirdsql://localhost:3050/C:/Users/KTOSIEK/Desktop/projekt_World_of/MILITARIA",
				"sysdba", "masterkey")) {
			grid.setConnection(connection);
			this.wyswietlGaraz();
		}
	}
}
